#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define TEST_HEAP_SIZE 65536
#define TEST_BLOCK_SIZE 1024

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void alloc_one_test() {
    void* block = _malloc(TEST_BLOCK_SIZE);
    assert(block != NULL && "Test 1 failed - failed to allocate one block");
    _free(block);
    printf("Test 1 passed\n");
}

void alloc_multiple_test() {
    void* blocks[5];
    for (int i = 0; i < 5; i++) blocks[i] = _malloc(TEST_BLOCK_SIZE);
    for (int i = 0; i < 5; i++) assert(blocks[i] != NULL && "Test 2 failed - failed to allocate multiple blocks");
    for (int i = 0; i < 5; i++) _free(blocks[i]);
    printf("Test 2 passed\n");
}

void free_one_test() {
    void* block = _malloc(TEST_BLOCK_SIZE);
    struct block_header* header = block_get_header(block);
    _free(block);
    assert(header->is_free && "Test 3 failed - failed to free one block");
    printf("Test 3 passed\n");
}

void free_multiple_test() {
    void* blocks[5];
    for (int i = 0; i < 5; i++) blocks[i] = _malloc(TEST_BLOCK_SIZE);
    for (int i = 0; i < 5; i++) _free(blocks[i]);
    for (int i = 0; i < 5; i++) {
        struct block_header* header = block_get_header(blocks[i]);
        assert(header->is_free && "Test 4 failed - failed to free multiple blocks");
    }
    printf("Test 4 passed\n");
}

void old_region_expandable_test() {
    size_t block_size_big = 2 * TEST_HEAP_SIZE;
    void* block = _malloc(block_size_big);
    struct block_header* header = block_get_header(block);
    assert(header->capacity.bytes >= block_size_big && "Test 5 failed - failed to expand the region");
    _free(block);
    printf("Test 5 passed\n");
}

void old_region_not_expandable_test() {
    size_t block_size_huge = 5 * TEST_HEAP_SIZE;
    void* block = _malloc(block_size_huge);
    struct block_header* header = block_get_header(block);
    while (header->next != NULL) header = header->next;
    size_t block_offset = size_from_capacity(header->capacity).bytes;
    void* expected_addr = header + block_offset;
    void* garbage_data = mmap(expected_addr, block_size_huge, PROT_READ, MAP_FIXED, -1, 0);
    void* new_block = _malloc(block_size_huge);
    struct block_header* new_header = block_get_header(block);
    assert(header != new_header && "Test 6 failed - region should be unable to expand");
    _free(block);
    _free(new_block);
    munmap(garbage_data, block_size_huge);
    printf("Test 6 passed\n");
}

void run_all_tests() {
    heap_init(TEST_HEAP_SIZE);
    alloc_one_test();
    alloc_multiple_test();
    free_one_test();
    free_multiple_test();
    old_region_expandable_test();
    old_region_not_expandable_test();
    printf("All tests passed\n");
}
